#!/usr/bin/env python3


from utility import indent


class Library:

    def __init__(self, name):
        self.name = name

    def to_string(self, n=0):
        return f"{indent(n)}library {self.name};"


class Use:

    def __init__(self, library, package, dot_all=False):
        self.library = library
        self.package = package
        self.dot_all = dot_all

    def to_string(self, n=0):
        dot_all = ".all" if self.dot_all else ""
        return f"{indent(n)}use {self.library}.{self.package}{dot_all};"
